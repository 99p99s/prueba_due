Due
├── example          <<< EXAMPLE PROGRAM (COULD BE NAMED "src" IN ANOTHER INSTANCE)
│   ├── Makefile     <<< THE PROJECT'S MAKEFILE
│   └── main.c       <<< THE ENTRY POINT, a.k.a THE MAIN
│
├── include                  <<< INCLUDE DIRECTORY FOR THE PROJECT
│   ├── due_sam3x.h          <<< WRAPPER TO INCLUDE ALL SAM/CSMIS
│   └── due_sam3x.init.h     <<< INCLUDE IN THE MAIN FOR INIT
│
├── lib
│   ├── libarm_cortexM3l_math.a          <<< ARM MATH LIB
│   ├── libsam_sam3x8e_gcc_rel.a         <<< BUILT LIBSAM FOR SAM3X8E
│   └── libsam_sam3x8e_gcc_rel.a.txt     <<< DUMPFILE OF THE LIBSAM CONTENTS
│
├── make
│   └── make_due_firmware.mk     <<< INCLUDE THIS MAKEFILE IN THE PROJECT'S MAKEFILE
│
├── sam                    <<< ARM/ATMEL/CSMIS INCLUDE DIRECTORY
│   ├── CMSIS              <<< CSMIS INCLUDES
│   ├── libsam             <<< ATMEL PERIPHERAL SPECIFIC INCLUDES
│   └── linker_scripts     <<< LINKER SCRIPTS
│
└── tools                     <<< TOOLCHAIN
    ├── bossac                <<< BOSSAC (BOSSA-UPLOADER FOR SAM-BA)
    └── arm-none-eabi-gcc     <<< GCC FOR ARM
