Messing With An Arduino Due. Or How I Try To Learn About Embedded Systems.

This is a gathering of files & tools that prove be useful while trying to program the Arduino Due. This board's main processor is one of the many incarnations that exists for the ARM Cortex-M3: an Atmel's ATSAM3X8E. For what I understand, Atmel has long be gonne since it had been acquired by Microchip. Nevermind about the corporate food chain, I grab all the info and free software that I was able to find.

Sources to get started:

https://atwillys.de/content/cc/using-custom-ide-and-system-library-on-arduino-due-sam3x8e

https://jacobmossberg.se/posts/2018/08/11/run-c-program-bare-metal-on-arm-cortex-m3.html

https://web.archive.org/web/20170407224104/http://inverseproblem.co.nz/Guides/index.php?n=ARM.Asfguide

https://forum.arduino.cc/t/how-to-debug-due/693033/6

https://mcuoneclipse.com/2021/05/01/visual-studio-code-for-c-c-with-arm-cortex-m-part-1/

https://mcuoneclipse.com/2021/05/04/visual-studio-code-for-c-c-with-arm-cortex-m-part-2/